local mod = RegisterMod("Eight", 1)


XalumMods = XalumMods or {}
XalumMods.Eight = mod

local json = require "json"

game = Game()
sfx = SFXManager()
music = MusicManager()

mod.DefaultKColor = KColor(1, 1, 1, 1)

mod.COLLECTIBLES = {
	BLISS_CIRCUIT 			= Isaac.GetItemIdByName("Bliss Circuit"),
	BLISS_CIRCUIT_EASY 		= Isaac.GetItemIdByName("Bliss Circuit (Easy)"),
	BLISS_CIRCUIT_MEDIUM 	= Isaac.GetItemIdByName("Bliss Circuit (Medium)"),
	BLISS_CIRCUIT_HARD 		= Isaac.GetItemIdByName("Bliss Circuit (Hard)"),

	METATRON				= Isaac.GetItemIdByName("Metatron"),
	GOLD_CUBE				= Isaac.GetItemIdByName("Gold-Cube"),
	ANTI_CUBE				= Isaac.GetItemIdByName("Anti-Cube"),
	HEART_CUBE				= Isaac.GetItemIdByName("Heart-Cube"),

	-- SHINY_LURE				= Isaac.GetItemIdByName("Shiny Lure"),
}

mod.CHARACTERS = {
	EIGHT 	= Isaac.GetPlayerTypeByName("Eight"),
}

mod.COSTUMES = {
	EIGHT_HEAD		= Isaac.GetCostumeIdByPath("gfx/characters/character_eight_head.anm2"),
}

mod.EFFECTS = {
	HACKING_RETICLE = Isaac.GetEntityVariantByName("Hacking Reticle"),
	THROWN_SPRITE 	= Isaac.GetEntityVariantByName("Misc Thrown Sprite"),
	NO_FISHING		= Isaac.GetEntityVariantByName("No Fishing Sign"),
}

mod.PICKUPS = {
	FLAT_COLLECTIBLE	= Isaac.GetEntityVariantByName("Flat Collectible"),
	METATRON_CUBE		= Isaac.GetEntityVariantByName("Metatron Cube"),
}

mod.CARDS = {
	BLOOD_JELLY		= Isaac.GetCardIdByName("Blood Jelly"),

	GOLD_FISH		= Isaac.GetCardIdByName("Gold Fish"),
	GRILLED_FISH	= Isaac.GetCardIdByName("Grilled Fish"),
	HEARTY_FISH		= Isaac.GetCardIdByName("Hearty Fish"),
	GULPER_EEL		= Isaac.GetCardIdByName("Gulper Eel"),
	DEAD_FISH		= Isaac.GetCardIdByName("Dead Fish"),
	CATFISH			= Isaac.GetCardIdByName("Catfish"),
	TECHNO_FISH		= Isaac.GetCardIdByName("Techno Fish"),

	ANGELFISH		= Isaac.GetCardIdByName("Angelfish"),
	DEVIL_RAY		= Isaac.GetCardIdByName("Devil Ray"),
	STARFISH		= Isaac.GetCardIdByName("Starfish"),
}

mod.CounterHUDFrame = { -- Frame number in gfx/ui/hangoutpack_hudpickups.anm2
	METATRON = 0,
}

mod.CounterHUDPriority = { -- Character gimmick counters should be rendered before item-added counters
	METATRON = 1,
}

function mod.AddTears(firedelay, val) -- Thanky you Dead & Kil <3 <3 <3
    local currentTears = 30 / (firedelay + 1)
    local newTears = currentTears + val
    return math.max((30 / newTears) - 1, -0.99)
end


function mod.GetDimensionID()
	local level = game:GetLevel()
	local id = level:GetCurrentRoomIndex()
	local hash = GetPtrHash(level:GetRoomByIdx(id))

	for i = 0, 2 do
		if hash == GetPtrHash(level:GetRoomByIdx(id, i)) then return i end
	end
end

function mod.IsInDimension(num)
	local level = game:GetLevel()
	local id = level:GetCurrentRoomIndex()

	return GetPtrHash(level:GetRoomByIdx(id)) == GetPtrHash(level:GetRoomByIdx(id, num or 0))
end

function mod.IsMirrorStage()
	local level = game:GetLevel()
	return level:GetAbsoluteStage() == LevelStage.STAGE1_2 and level:GetStageType() >= StageType.STAGETYPE_REPENTANCE
end

function mod.IsInMirror() return mod.IsMirrorStage() and mod.IsInDimension(1) end

function mod.AnyPlayerHas(ItemID)		for i = 1, game:GetNumPlayers() do if Isaac.GetPlayer(i - 1):HasCollectible(ItemID)			then return Isaac.GetPlayer(i - 1) end end end
function mod.AnyPlayerHasCard(CardID) 	for i = 1, game:GetNumPlayers() do if mod.PlayerHasCard(Isaac.GetPlayer(i - 1), CardID) 	then return Isaac.GetPlayer(i - 1) end end end
function mod.AnyPlayerIs(CharacterID) 	for i = 1, game:GetNumPlayers() do if Isaac.GetPlayer(i - 1):GetPlayerType() == CharacterID then return Isaac.GetPlayer(i - 1) end end end

function mod.PlayerHasCard(Player, CardID) return Player:GetCard(0) == CardID or Player:GetCard(1) == CardID end
function mod.PlayerHasActiveItem(Player, ItemID) for i = 0, 3 do if Player:GetActiveItem(i) == ItemID then return i end end end

function mod.MathLimit(n, lower, upper) return math.max(math.min(n, upper), lower) end

mod.DoCustomHUD = nil
mod.CustomHUDS = {}
function mod.AddCustomHUDCounter(Enumerator, DataName, CheckName)
	DataName = DataName or Enumerator
	CheckName = CheckName or Enumerator
	Enumerator = string.upper(Enumerator)

	mod.CustomHUDS[CheckName] = {Sprite(), Font(), mod.CounterHUDPriority[Enumerator], DataName}
	mod.DoCustomHUD = true

	mod.CustomHUDS[CheckName][1]:Load("gfx/ui/hangoutpack_hudpickups.anm2", true)
	mod.CustomHUDS[CheckName][1]:SetFrame("Idle", mod.CounterHUDFrame[Enumerator])

	mod.CustomHUDS[CheckName][2]:Load("font/pftempestasevencondensed.fnt")
end
function mod.CustomHUDExists(CheckName) return mod.CustomHUDS[CheckName] end

mod:AddCallback(ModCallbacks.MC_POST_RENDER, function()
	if mod.DoCustomHUD then
		local player = Isaac.GetPlayer()
		local data = player:GetData()
		local basepos = Vector(32, 32)

		local shiftInt = Options.HUDOffset * 10
		local shift = Vector(shiftInt * 2, shiftInt * 1.2)

		local mul = 0

		if CollectibleType.COLLECTIBLE_DADS_WALLET and mod.AnyPlayerHas(CollectibleType.COLLECTIBLE_DADS_WALLET) and FiendFolio.getField(FiendFolio.savedata, 'run', 'dadsdebt') and FiendFolio.getField(FiendFolio.savedata, 'run', 'dadsdebt') > 0 then
			mul = 1
		elseif mod.AnyPlayerHas(CollectibleType.COLLECTIBLE_DEEP_POCKETS) then
			basepos = basepos + Vector(6, 0)
		end

		local priorities = {}
		for _, value in pairs(mod.CustomHUDS) do
			priorities[value[3]] = priorities[value[3]] or {}
			priorities[value[3]][#priorities[value[3]] + 1] = {value[1], value[2], value[4]}
		end

		local offset = Vector(0, 12)
		for priority, value in pairs(priorities) do
			for _, tab in pairs(value) do
				local n = data[tab[3]] or 0
				if n < 10 then
					n = "0" .. n
				else
					n = tostring(n)
				end

				local pos = basepos + shift + offset * mul

				tab[1]:Render(pos, Vector.Zero, Vector.Zero)
				tab[2]:DrawString(n, pos.X + 18, pos.Y + 1, mod.DefaultKColor)

				mul = mul + 1
			end
		end
	end
end)

mod.PersistentEntities = {}
function mod.AddPersistentEntity(Entity)
	local roomidx = game:GetLevel():GetCurrentRoomDesc().SafeGridIndex

	mod.PersistentEntities[roomidx] = mod.PersistentEntities[roomidx] or {}
	mod.PersistentEntities[roomidx][#mod.PersistentEntities[roomidx] + 1] = {Entity.Position, Entity.Type, Entity.Variant, Entity.SubType}
end

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	local level = game:GetLevel()
	if game:GetRoom():IsFirstVisit() and level:GetCurrentRoomDesc().SafeGridIndex == level:GetRoomByIdx(level:GetStartingRoomIndex()).SafeGridIndex then
		mod.PersistentEntities = {}
	end

	local roomidx = level:GetCurrentRoomDesc().SafeGridIndex
	if mod.PersistentEntities[roomidx] then
		for _, tab in pairs(mod.PersistentEntities[roomidx]) do
			Isaac.Spawn(tab[2], tab[3] or 0, tab[4] or 0, tab[1] or Vector.Zero, Vector.Zero, nil)
		end
	end
end)

mod.QueuedEntities = {}
function mod.QueueSpawn(typ, var, sub, pos, vel, spawner)
	table.insert(mod.QueuedEntities, {typ, var, sub, pos, vel, spawner})
end

mod:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	for i, v in pairs(mod.QueuedEntities) do
		if #Isaac.FindInRadius(v[4], 40, EntityPartition.PLAYER) <= 0 then
			Isaac.Spawn(v[1], v[2], v[3], v[4], v[5] or Vector.Zero, v[6])
			table.remove(mod.QueuedEntities, i)
			break
		end
	end
end)

function mod.SaveSave()
	local players = Isaac.FindByType(1)

	mod.Save.PersistentEntities			= mod.PersistentEntities

	mod.Save.HackingLockedSecretDoors	= mod.HackingLockedSecretDoors
	mod.Save.HackingOverrideCurse		= mod.HackingOverrideCurse
	mod.Save.HackingOverrideCurseDouble = mod.HackingOverrideCurseDouble
	mod.Save.CorpseHoleFishingCounter	= mod.CorpseHoleFishingCounter
	mod.Save.FishedWitnessKnife			= mod.FishedWitnessKnife

	mod.Save.CustomAngelRooms			= mod.CustomAngelRooms
	mod.Save.CustomDevilRooms			= mod.CustomDevilRooms
	mod.Save.HasCustomAngelRoom			= mod.HasCustomAngelRoom
	mod.Save.HasCustomDevilRoom			= mod.HasCustomDevilRoom

	mod.Save.MetatronCubes				= players[1]:GetData().metatron

	mod:SaveData(json.encode(mod.Save))
end

function mod.ResetSave()
	mod.Save.PersistentEntities			= {}

	mod.Save.HackingLockedSecretDoors	= {}
	mod.Save.HackingOverrideCurse		= nil
	mod.Save.HackingOverrideCurseDouble = nil
	mod.Save.CorpseHoleFishingCounter	= 0
	mod.Save.FishedWitnessKnife			= false

	mod.Save.MetatronCubes				= 0
end

function mod.LoadSaveGlobals()
	mod.PersistentEntities			= mod.Save.PersistentEntities

	mod.HackingLockedSecretDoors	= mod.Save.HackingLockedSecretDoors
	mod.HackingOverrideCurse		= mod.Save.HackingOverrideCurse
	mod.HackingOverrideCurseDouble	= mod.Save.HackingOverrideCurseDouble
	mod.CorpseHoleFishingCounter	= mod.Save.CorpseHoleFishingCounter
	mod.FishedWitnessKnife			= mod.Save.FishedWitnessKnife
end

function mod.LoadSavePlayer()
	local players = Isaac.FindByType(1)
	players[1]:GetData().metatron	= mod.Save.MetatronCubes
end

function mod.InitaliseConfigOptions()
	mod.BlissCircuitSlot = mod.BlissCircuitSlot or ActiveSlot.SLOT_POCKET
end

mod.Save = mod.Save or {}
if not mod:HasData() then
	mod:SaveData(json.encode({}))
end

mod.Save = json.decode(mod:LoadData())
mod.LoadSaveGlobals()
mod.InitaliseConfigOptions()

mod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, save)
	if save then
		mod.LoadSavePlayer()
	else
		mod.ResetSave()
	end

	mod.DoCustomHUD = nil
	mod.CustomHUDS = {}
	mod.GameStarted = true
end)

mod:AddCallback(ModCallbacks.MC_POST_GAME_END, function()
	mod.ResetSave()
	mod:SaveData(json.encode(mod.Save))

	mod.GameStarted = false
end)

mod:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, function(_, should)
	if should then mod.SaveSave() end

	mod.GameStarted = false
end)

mod:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function()
	if mod.GameStarted then
		mod.SaveSave()
	end
end)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	mod.ApproxDamageMultiplier = (player.Damage > 3.5 and 3.5 / player.Damage or player.Damage / 3.5)
end, CacheFlag.CACHE_DAMAGE)

mod.toload = {
	"meta.dss",

	"eight",

	"items.metatron",
	"items.bliss_circuit",
}

for _, file in pairs(mod.toload) do
	include("lua/" .. file)
end