local mod = XalumMods.Eight

mod.GoldCubeSecretChance		= 1/15
mod.GoldCubeChestChance			= 1/4
mod.AntiCubeSuperSecretChance	= 1/3
mod.HeartCubeUltraSecretChance	= 1/4
mod.HeartCubeChestChance		= 1/10

mod.MetatronReplacables = {
	[20] = true,
	[30] = true,
	[40] = true,
	[90] = true,
	[69] = true,
	[300] = true,
}

function mod.AwardMetatronCube(player)
	local rng = player:GetCollectibleRNG(mod.COLLECTIBLES.METATRON)
	local cube = rng:RandomInt((1 + mod.COLLECTIBLES.HEART_CUBE - mod.COLLECTIBLES.GOLD_CUBE) * 2) % 3 + mod.COLLECTIBLES.GOLD_CUBE

	local item = Isaac.GetItemConfig():GetCollectible(cube)
	local hud = game:GetHUD()

	player:QueueItem(item)
	player:AnimateCollectible(cube, "Pickup", "PlayerPickupSparkle")
	
	hud:ShowItemText(item.Name, item.Description)

	sfx:Play(SoundEffect.SOUND_POWERUP1, 1, 0, false, 1)
end

mod:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, player)
	if player:HasCollectible(mod.COLLECTIBLES.METATRON) and not mod.CustomHUDExists("metatron") then
		mod.AddCustomHUDCounter("metatron")
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, function(_, pickup)
	if pickup:GetSprite():IsPlaying("Appear") and pickup.SpawnerType ~= 1 then
		if mod.MetatronReplacables[pickup.Variant] and mod.AnyPlayerHas(mod.COLLECTIBLES.METATRON) then
			local rng = Isaac.GetPlayer():GetCollectibleRNG(mod.COLLECTIBLES.METATRON)

			local limit = pickup.Variant == 20 and 10 or 20
			if rng:RandomInt(100) < limit then
				pickup:Morph(5, mod.PICKUPS.METATRON_CUBE, 0, true, true)
				return
			end
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_PRE_GET_COLLECTIBLE, function(_, pool, decrease, seed)
	if not mod.MetatronTurnabout and mod.AnyPlayerHas(mod.COLLECTIBLES.METATRON) then
		local roomtype = game:GetRoom():GetType()
		local rng = RNG()
		rng:SetSeed(seed, 46)

		if pool == ItemPoolType.POOL_GOLDEN_CHEST then
			if rng:RandomFloat() < mod.GoldCubeChestChance then
				mod.MetatronTurnabout = true
				return mod.COLLECTIBLES.GOLD_CUBE
			end
		elseif pool == ItemPoolType.POOL_RED_CHEST then
			if rng:RandomFloat() < mod.HeartCubeChestChance then
				mod.MetatronTurnabout = true
				return mod.COLLECTIBLES.HEART_CUBE
			end
		elseif pool == ItemPoolType.POOL_SECRET then
			if roomtype == RoomType.ROOM_SECRET then
				if rng:RandomFloat() < mod.GoldCubeSecretChance then
					mod.MetatronTurnabout = true
					return mod.COLLECTIBLES.GOLD_CUBE
				end
			elseif roomtype == RoomType.ROOM_SUPERSECRET then
				if rng:RandomFloat() < mod.AntiCubeSuperSecretChance then
					mod.MetatronTurnabout = true
					return mod.COLLECTIBLES.ANTI_CUBE
				end
			end
		elseif pool == ItemPoolType.POOL_ANGEL and roomtype == RoomType.ROOM_ULTRASECRET then
			if rng:RandomFloat() < mod.HeartCubeUltraSecretChance then
				mod.MetatronTurnabout = true
				return mod.COLLECTIBLES.HEART_CUBE
			end
		end
	end

	mod.MetatronTurnabout = nil
end)

mod:AddCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, function(_, pickup, collider)
	local sprite = pickup:GetSprite()
	if (sprite:GetAnimation() == "Idle" or sprite:WasEventTriggered("DropSound")) and collider.Type == 1 then
		local p1 = Isaac.GetPlayer()
		local data = p1:GetData()

		data.metatron = data.metatron and math.min(99, data.metatron + 1) or 1
		pickup.EntityCollisionClass = 0

		sprite:Play("Collect")
		sfx:Play(SoundEffect.SOUND_SCAMPER)

		if data.metatron >= 8 then
			mod.AwardMetatronCube(collider:ToPlayer())
			data.metatron = data.metatron - 8
		end

		return true
	end

	if sprite:IsEventTriggered("DropSound") then sfx:Play(SoundEffect.SOUND_SCAMPER) end
	if sprite:IsFinished("Collect") then pickup:Remove() end
end, mod.PICKUPS.METATRON_CUBE)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pickup)
	local sprite = pickup:GetSprite()
	if sprite:IsEventTriggered("DropSound") then sfx:Play(SoundEffect.SOUND_SCAMPER) end
	if sprite:IsFinished("Collect") then pickup:Remove() end
end, mod.PICKUPS.METATRON_CUBE)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	player.Damage = player.Damage + 0.25 * player:GetCollectibleNum(mod.COLLECTIBLES.GOLD_CUBE) * mod.ApproxDamageMultiplier
	player.Damage = player.Damage + 0.15 * player:GetCollectibleNum(mod.COLLECTIBLES.HEART_CUBE) * mod.ApproxDamageMultiplier
end, CacheFlag.CACHE_DAMAGE)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	player.MaxFireDelay = mod.AddTears(player.MaxFireDelay, 0.5 * player:GetCollectibleNum(mod.COLLECTIBLES.ANTI_CUBE))
	player.MaxFireDelay = mod.AddTears(player.MaxFireDelay, 0.2 * player:GetCollectibleNum(mod.COLLECTIBLES.HEART_CUBE))
end, CacheFlag.CACHE_FIREDELAY)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	player.MoveSpeed = player.MoveSpeed + 0.1 * player:GetCollectibleNum(mod.COLLECTIBLES.HEART_CUBE)
end, CacheFlag.CACHE_SPEED)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	player.ShotSpeed = player.ShotSpeed + 0.1 * player:GetCollectibleNum(mod.COLLECTIBLES.HEART_CUBE)
end, CacheFlag.CACHE_SHOTSPEED)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	player.Luck = player.Luck + 0.5 * player:GetCollectibleNum(mod.COLLECTIBLES.HEART_CUBE)
end, CacheFlag.CACHE_LUCK)