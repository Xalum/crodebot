local mod = XalumMods.Eight

mod.IsHacking = mod.IsHacking or false
mod.HighestFish = mod.CARDS.TECHNO_FISH
mod.HighestSpecialFish = mod.CARDS.STARFISH

mod.HackingLockedSecretDoors = {}

mod.HackingTileType = {
	TILE_BLANK	 	= 0,
	TILE_PLAYER	 	= 1,
	TILE_SPIKE	 	= 2,
	TILE_DOOR	 	= 3,
	TILE_CROWN	 	= 4,
	TILE_HOOK	 	= 5,
	TILE_FISH 	 	= 6,
	TILE_JELLY	 	= 7,
	TILE_GOLDFISH	= 8,
}

mod.HackingSecretDoorMatch = {
	[DoorSlot.LEFT0] = 1 << 2,
	[DoorSlot.LEFT1] = 1 << 2,
	[DoorSlot.RIGHT0] = 1,
	[DoorSlot.RIGHT1] = 1,
	[DoorSlot.UP0] = 1 << 3,
	[DoorSlot.UP1] = 1 << 3,
	[DoorSlot.DOWN0] = 1 << 1,
	[DoorSlot.DOWN1] = 1 << 1,
}

mod.HackingSecretDoorSwapper = {
	[DoorSlot.LEFT0] = DoorSlot.RIGHT0,
	[DoorSlot.LEFT1] = DoorSlot.RIGHT0,
	[DoorSlot.RIGHT0] = DoorSlot.LEFT0,
	[DoorSlot.RIGHT1] = DoorSlot.LEFT0,
	[DoorSlot.UP0] = DoorSlot.DOWN0,
	[DoorSlot.UP1] = DoorSlot.DOWN0,
	[DoorSlot.DOWN0] = DoorSlot.UP0,
	[DoorSlot.DOWN1] = DoorSlot.UP0,
}

mod.FishSprites = {
	[mod.CARDS.BLOOD_JELLY]		= "gfx/items/pick ups/pickup_fish_jelly_blood.png",

	[mod.CARDS.GOLD_FISH]		= "gfx/items/pick ups/pickup_fish_gold.png",
	[mod.CARDS.GRILLED_FISH]	= "gfx/items/pick ups/pickup_fish_grilled.png",
	[mod.CARDS.HEARTY_FISH]		= "gfx/items/pick ups/pickup_fish_hearty.png",
	[mod.CARDS.GULPER_EEL]		= "gfx/items/pick ups/pickup_fish_gulper.png",
	[mod.CARDS.DEAD_FISH]		= "gfx/items/pick ups/pickup_fish_dead.png",
	[mod.CARDS.CATFISH]			= "gfx/items/pick ups/pickup_fish_cat.png",
	[mod.CARDS.TECHNO_FISH]		= "gfx/items/pick ups/pickup_fish_techno.png",

	[mod.CARDS.ANGELFISH]		= "gfx/items/pick ups/pickup_fish_angel.png",
	[mod.CARDS.DEVIL_RAY]		= "gfx/items/pick ups/pickup_fish_ray_devil.png",
	[mod.CARDS.STARFISH]		= "gfx/items/pick ups/pickup_fish_star.png",
}

function mod.GatherHackingTargets()
	-- Targets:
		-- ✓ Locked doors
		-- ✓ Item pedestals
		-- ✓ Shop Items
		-- ✓ Devil/black market deals
		--   Pits
		-- ✓ Curse room doors
		-- ✓ Secret room doors

	mod.HackingTargets = {}

	local room = game:GetRoom()
	local level = game:GetLevel()

	local size = room:GetGridSize()
	local roomtype = room:GetType()

	local g
	local t
	local canfish = #Isaac.FindByType(1000, mod.EFFECTS.NO_FISHING) == 0

	for i = 0, size do
		g = room:GetGridEntity(i)
		if g then
			t = g:GetType()
			if (t == GridEntityType.GRID_PIT and canfish) or (t == GridEntityType.GRID_LOCK and g.State == 0) or (t == GridEntityType.GRID_TRAPDOOR and canfish and level:GetAbsoluteStage() == LevelStage.STAGE4_2 and level:GetStageType() == StageType.STAGETYPE_REPENTANCE and roomtype == RoomType.ROOM_BOSS) then
				mod.HackingTargets[#mod.HackingTargets + 1] = g
			end
		end
	end

	local d
	for i = 0, 8 do
		if room:IsDoorSlotAllowed(i) then
			d = room:GetDoor(i)
			if d and (d:IsLocked() or (d:IsRoomType(RoomType.ROOM_CURSE) and d.VarData == 0 and not mod.HackingOverrideCurseDouble) or (d:CanBlowOpen() and roomtype ~= RoomType.ROOM_SECRET)) then
				mod.HackingTargets[#mod.HackingTargets + 1] = d
			end
		end
	end
end

function mod.CheckHackingTargets()
	if not mod.DidGatherTargets then
		mod.DidGatherTargets = mod.AnyPlayerHas(mod.COLLECTIBLES.BLISS_CIRCUIT) or mod.AnyPlayerHasCard(mod.CARDS.TECHNO_FISH)
		if mod.DidGatherTargets then
			mod.DidGatherTargets:GetData().HackingReticle = Isaac.Spawn(1000, mod.EFFECTS.HACKING_RETICLE, 0, Vector.Zero, Vector.Zero, nil)

			mod.GatherHackingTargets()
		end
	end
end

function mod.GetClosestHackableTarget(position)
	local closest = nil
	local maxdist = 120

	local all = {}
	for _, value in pairs(mod.HackingTargets) do all[#all + 1] = value end
	for _, value in pairs(mod.HackingPickups) do all[#all + 1] = value end

	for _, entity in pairs(all) do
		if not (entity.Friction and entity.Variant == 100 and entity.SubType == 0) then
			local dist = entity.Position:Distance(position)

			if dist < maxdist or (not entity.Friction and entity:ToDoor() and dist < maxdist * 1.5) then
				closest = entity
				maxdist = dist
			end
		end
	end

	return closest
end

function mod.PerformSuccessfulHack(player)
	local data = player:GetData()
	if not data.HackingTarget then return end

	if data.HackingTarget.Friction then -- Entity
		local pickup = data.HackingTarget:ToPickup()

		if pickup then
			local sprite = pickup:GetSprite()

			if pickup.Variant == 100 and not (sprite:IsPlaying("ShopIdle") or sprite:IsFinished("ShopIdle")) then
				pickup:Morph(5, 100, 0, true, true)
			end

			if pickup.Price > 0 then
				pickup.AutoUpdatePrice = false
				pickup.Price = math.floor(pickup.Price * 0.5)
			end

			if pickup.Price == -2 then
				pickup.AutoUpdatePrice = false
				pickup.Price = -1

				for index, value in pairs(mod.HackingPickups) do
					if value.InitSeed == pickup.InitSeed then
						table.remove(mod.HackingPickups, index)
						break
					end
				end
			end
		end
	else -- GridEntity
		local door = data.HackingTarget:ToDoor()

		if door then
			if door:IsLocked() then
				door:TryUnlock(player, true)
			elseif door:CanBlowOpen() then
				door:TryBlowOpen(true, player)
			elseif door:IsRoomType(RoomType.ROOM_CURSE) then
				door.VarData = 1

				local sprite = door:GetSprite()
				sprite:ReplaceSpritesheet(3, "gfx/grid/door_04_selfsacrificeroomdoor_nospikes.png")
				sprite:LoadGraphics()

				mod.HackingOverrideCurse = true
			end
		else
			local pit = data.HackingTarget:ToPit()
			local rng = Isaac.GetPlayer():GetCollectibleRNG(mod.COLLECTIBLES.BLISS_CIRCUIT)

			if not pit then
				if data.HackingTarget:GetType() == GridEntityType.GRID_LOCK then
					data.HackingTarget:Destroy()
				end 
			end
		end
	end

	mod.GatherHackingTargets()
end

function mod.PerformFailedHack(player)
	local data = player:GetData()
	if not data.HackingTarget then return end

	if data.HackingTarget.Friction then -- Entity
		local pickup = data.HackingTarget:ToPickup()

		if pickup then
			local sprite = pickup:GetSprite()

			if pickup.Variant == 100 and not (sprite:IsPlaying("ShopIdle") or sprite:IsFinished("ShopIdle")) then
				pickup:Remove()
				Isaac.Spawn(1000, 15, 0, pickup.Position, Vector.Zero, nil)

				for index, value in pairs(mod.HackingPickups) do
					if value.InitSeed == pickup.InitSeed then
						table.remove(mod.HackingPickups, index)
						break
					end
				end
			end

			if pickup.Price > 0 then
				pickup.AutoUpdatePrice = false
				pickup.Price = math.floor(pickup.Price * 2)
			end

			if pickup.Price == -2 then
				mod.HackingPickups = {}

				for _, p in pairs(Isaac.FindByType(5, 100)) do
					local sprite = p:GetSprite()
					if (sprite:IsPlaying("ShopIdle") or sprite:IsFinished("ShopIdle")) then
						p:Remove()
						Isaac.Spawn(1000, 15, 0, p.Position, Vector.Zero, nil)
					end
				end
			end
		end
	else -- GridEntity
		local door = data.HackingTarget:ToDoor()

		if door then
			if door:IsLocked() then
				if door:GetVariant() ~= 2 then
					local state = door:GetSaveState()
					state.Variant = 2

					local sprite = door.ExtraSprite
					sprite:Load("gfx/grid/door_16_doublelock.anm2", true)
					sprite:Play("TwoChains")
					door.ExtraSprite = sprite

					door.ExtraVisible = true
				end
			elseif door:CanBlowOpen() then
				local state = door:GetSaveState()
				state.Variant = 17

				local sprite = door:GetSprite()
				sprite:Load("gfx/grid/door_17_bardoor.anm2", true)
				sprite:Play("Appear")

				sprite.Offset = Vector(13, 0):Rotated(90 * door.Direction)

				local level = game:GetLevel()
				local room2 = level:GetRoomByIdx(door.TargetRoomIndex)

				door:Init(state.VariableSeed)

				local roomidx = level:GetCurrentRoomDesc().SafeGridIndex
				mod.HackingLockedSecretDoors[roomidx] = mod.HackingLockedSecretDoors[roomidx] or {}

				mod.HackingLockedSecretDoors[roomidx][#mod.HackingLockedSecretDoors[roomidx] + 1] = door.Slot

				local mask = mod.HackingSecretDoorMatch[door.Slot]
				if room2.Data.Doors & mask > 0 then
					local roomidx2 = room2.SafeGridIndex
					mod.HackingLockedSecretDoors[roomidx2] = mod.HackingLockedSecretDoors[roomidx2] or {}

					mod.HackingLockedSecretDoors[roomidx2][#mod.HackingLockedSecretDoors[roomidx2] + 1] = mod.HackingSecretDoorSwapper[door.Slot]
				end
				
			elseif door:IsRoomType(RoomType.ROOM_CURSE) then
				local sprite = door:GetSprite()
				sprite:ReplaceSpritesheet(3, "gfx/grid/super_cursed_room.png")
				sprite:LoadGraphics()

				mod.HackingOverrideCurseDouble = true
			end
		else
			local pit = data.HackingTarget:ToPit()

			if not pit then
				if data.HackingTarget:GetType() == GridEntityType.GRID_LOCK then
					local index = data.HackingTarget:GetGridIndex()
					local room = game:GetRoom()

					room:RemoveGridEntity(index, 0, false)
					room:Update()
					room:SpawnGridEntity(index, GridEntityType.GRID_PILLAR, 0, math.random(10), 0)
				else
					mod.AddPersistentEntity(Isaac.Spawn(1000, mod.EFFECTS.NO_FISHING, 0, data.HackingTarget.Position, Vector.Zero, nil))
				end

				Isaac.Spawn(1000, 15, 0, data.HackingTarget.Position + Vector(0, 1), Vector.Zero, nil)
			end
		end
	end

	mod.GatherHackingTargets()
end

function mod.ValidateFishedPosition(entity, throwheight, player)
	local room = game:GetRoom()
	local vel = entity.Velocity

	local frames = 1 + throwheight * 2
	local target = entity.Position + vel * frames

	local coll = room:GetGridCollisionAtPos(target)

	if coll ~= GridCollisionClass.COLLISION_NONE then
		target = room:FindFreePickupSpawnPosition(player.Position + RandomVector():Resized(40))
		vel = (target - entity.Position) / frames
	end

	return vel
end

function mod.FishingRewards(player, tile)
	local data = player:GetData()
	if not data.HackingTarget then return end
	
	mod.IsHacking = false
	data.blisscircuit = false
	player.ControlsEnabled = true

	local pit = data.HackingTarget:ToPit()

	local rng = Isaac.GetPlayer():GetCollectibleRNG(mod.COLLECTIBLES.BLISS_CIRCUIT)
	local roll = rng:RandomInt(100)

	if tile == mod.HackingTileType.TILE_SPIKE then
		mod.AddPersistentEntity(Isaac.Spawn(1000, mod.EFFECTS.NO_FISHING, 0, data.HackingTarget.Position, Vector.Zero, nil))
		Isaac.Spawn(1000, 15, 0, data.HackingTarget.Position + Vector(0, 1), Vector.Zero, nil)

		mod.GatherHackingTargets()

		return
	end

	if pit then
		Isaac.Spawn(1000, 132, 0, pit.Position, Vector.Zero, nil).SpriteScale = Vector(0.5, 0.5)
		sfx:Play(SoundEffect.SOUND_BOSS2INTRO_WATER_EXPLOSION)

		if tile == mod.HackingTileType.TILE_JELLY then
			player:AnimateSad()

			mod.AddPersistentEntity(Isaac.Spawn(1000, mod.EFFECTS.NO_FISHING, 0, pit.Position, Vector.Zero, nil))
			Isaac.Spawn(1000, 15, 0, data.HackingTarget.Position + Vector(0, 1), Vector.Zero, nil)

			local thrown = Isaac.Spawn(1000, mod.EFFECTS.THROWN_SPRITE, 2, pit.Position, (player.Position - pit.Position):Resized(5):Rotated(rng:RandomInt(51) - 25), nil)
			thrown.SpriteRotation = rng:RandomInt(360)

			local throwheight = 10
			thrown.Velocity = mod.ValidateFishedPosition(thrown, throwheight, player)

			local id = mod.CARDS.BLOOD_JELLY

			thrown:GetSprite():ReplaceSpritesheet(0, mod.FishSprites[id])
			thrown:GetSprite():LoadGraphics()

			local td = thrown:GetData()
			td.OffsetAccel = -throwheight
			td.fish = id
		else
			player:AnimateHappy()

			if tile == mod.HackingTileType.TILE_GOLDFISH then
				roll = 1

				if roll < 60 then
					local thrown = Isaac.Spawn(1000, mod.EFFECTS.THROWN_SPRITE, 2, pit.Position, (player.Position - pit.Position):Resized(5):Rotated(rng:RandomInt(51) - 25), nil)
					thrown.SpriteRotation = rng:RandomInt(360)

					local throwheight = 10
					thrown.Velocity = mod.ValidateFishedPosition(thrown, throwheight, player)

					local id = rng:RandomInt(1 + mod.HighestSpecialFish - mod.CARDS.ANGELFISH) + mod.CARDS.ANGELFISH

					thrown:GetSprite():ReplaceSpritesheet(0, mod.FishSprites[id])
					thrown:GetSprite():LoadGraphics()

					local td = thrown:GetData()
					td.OffsetAccel = -throwheight
					td.fish = id
				elseif roll < 95 then -- Collectible
					local itempool = game:GetItemPool()
					local roomtype = game:GetRoom():GetType()
					local pool = itempool:GetPoolForRoom(roomtype, 0)
					local id = itempool:GetCollectible(math.max(pool, 0), true)

					local thrown = Isaac.Spawn(1000, mod.EFFECTS.THROWN_SPRITE, 0, pit.Position, (player.Position - pit.Position):Resized(5):Rotated(rng:RandomInt(51) - 25), nil)
					thrown.SpriteRotation = rng:RandomInt(360)

					local throwheight = 10
					thrown.Velocity = mod.ValidateFishedPosition(thrown, throwheight, player)

					thrown:GetSprite():ReplaceSpritesheet(0, Isaac.GetItemConfig():GetCollectible(id).GfxFileName)
					thrown:GetSprite():LoadGraphics()

					local td = thrown:GetData()
					td.OffsetAccel = -throwheight
					td.item = id
				else -- Wormwood
					local wormwood = Isaac.Spawn(62, 3, 0, Vector.Zero, Vector.Zero, nil)
					wormwood:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
					wormwood:AddEntityFlags(EntityFlag.FLAG_HIDE_HP_BAR)

					repeat
						wormwood:Update()
					until wormwood:ToNPC().State == NpcState.STATE_JUMP

					wormwood.Position = pit.Position
					wormwood.Velocity = (player.Position - wormwood.Position):Resized(wormwood.Velocity:Length())

					local child = wormwood.Child

					repeat
						child.Position = pit.Position
						child = child.Child
					until not child
				end
			else
				local thrown = Isaac.Spawn(1000, mod.EFFECTS.THROWN_SPRITE, 2, pit.Position, (player.Position - pit.Position):Resized(5):Rotated(rng:RandomInt(51) - 25), nil)
				thrown.SpriteRotation = rng:RandomInt(360)

				local throwheight = 10
				thrown.Velocity = mod.ValidateFishedPosition(thrown, throwheight, player)

				local id = rng:RandomInt(1 + mod.HighestFish - mod.CARDS.GOLD_FISH) + mod.CARDS.GOLD_FISH

				thrown:GetSprite():ReplaceSpritesheet(0, mod.FishSprites[id])
				thrown:GetSprite():LoadGraphics()

				local td = thrown:GetData()
				td.OffsetAccel = -throwheight
				td.fish = id
			end
		end
	else
		if tile == mod.HackingTileType.TILE_JELLY then
			player:AnimateSad()

			mod.AddPersistentEntity(Isaac.Spawn(1000, mod.EFFECTS.NO_FISHING, 0, data.HackingTarget.Position, Vector.Zero, nil))
			Isaac.Spawn(1000, 15, 0, data.HackingTarget.Position + Vector(0, 1), Vector.Zero, nil)

			local thrown = Isaac.Spawn(1000, mod.EFFECTS.THROWN_SPRITE, 2, data.HackingTarget.Position, (player.Position - data.HackingTarget.Position):Resized(5):Rotated(rng:RandomInt(51) - 25), nil)
			thrown.SpriteRotation = rng:RandomInt(360)

			local throwheight = 10
			thrown.Velocity = mod.ValidateFishedPosition(thrown, throwheight, player)

			local id = mod.CARDS.BLOOD_JELLY

			thrown:GetSprite():ReplaceSpritesheet(0, mod.FishSprites[id])
			thrown:GetSprite():LoadGraphics()

			local td = thrown:GetData()
			td.OffsetAccel = -throwheight
			td.fish = id
		else
			player:AnimateHappy()

			if tile == mod.HackingTileType.TILE_FISH then
				local thrown = Isaac.Spawn(1000, mod.EFFECTS.THROWN_SPRITE, 1, data.HackingTarget.Position, (player.Position - data.HackingTarget.Position):Resized(5):Rotated(rng:RandomInt(51) - 25), nil)
				thrown:GetSprite():ReplaceSpritesheet(0, "gfx/effects/thrown_chest.png")
				thrown:GetSprite():LoadGraphics()

				local throwheight = 8
				thrown.Velocity = mod.ValidateFishedPosition(thrown, throwheight, player)

				local td = thrown:GetData()
				td.OffsetAccel = -throwheight

				mod.CorpseHoleFishingCounter = mod.CorpseHoleFishingCounter + 1

				if mod.CorpseHoleFishingCounter >= 4 then
					mod.AddPersistentEntity(Isaac.Spawn(1000, mod.EFFECTS.NO_FISHING, 0, data.HackingTarget.Position, Vector.Zero, nil))
					Isaac.Spawn(1000, 15, 0, data.HackingTarget.Position + Vector(0, 1), Vector.Zero, nil)
				end
			elseif tile == mod.HackingTileType.TILE_GOLDFISH then
				local thrown = Isaac.Spawn(1000, mod.EFFECTS.THROWN_SPRITE, 0, data.HackingTarget.Position, (player.Position - data.HackingTarget.Position):Resized(5):Rotated(rng:RandomInt(51) - 25), nil)
				thrown.SpriteRotation = rng:RandomInt(360)

				local throwheight = 10
				thrown.Velocity = mod.ValidateFishedPosition(thrown, throwheight, player)

				thrown:GetSprite():ReplaceSpritesheet(0, Isaac.GetItemConfig():GetCollectible(CollectibleType.COLLECTIBLE_MOMS_KNIFE).GfxFileName)
				thrown:GetSprite():LoadGraphics()

				local td = thrown:GetData()
				td.OffsetAccel = -throwheight
				td.item = CollectibleType.COLLECTIBLE_MOMS_KNIFE

				mod.FishedWitnessKnife = true
			end
		end
	end

	mod.GatherHackingTargets()
end

function mod.GenerateHackingGrid(fishing, treasure)
	if fishing then
		for i = 1, 7 do
			for j = 1, 7 do
				mod.HackingTiles[i][j]:SetFrame(mod.HackingTileType.TILE_BLANK)
			end
		end

		for i = 1, 7 do
			mod.HackingTiles[i][1]:SetFrame(mod.HackingTileType.TILE_SPIKE)
			mod.HackingTiles[i][7]:SetFrame(mod.HackingTileType.TILE_SPIKE)
		end

		mod.HackingTiles[4][3]:SetFrame(mod.HackingTileType.TILE_HOOK)
		mod.HackingPlayer = Vector(4, 3)
	else
		local pool = treasure and mod.HackingLayouts.Treasure or mod.HackingLayouts.Default
		local rng = Isaac.GetPlayer():GetCollectibleRNG(mod.COLLECTIBLES.BLISS_CIRCUIT)

		local roll = 0
		repeat
			roll = rng:RandomInt(#pool) + 1
		until roll ~= mod.LastHackingGrid

		mod.LastHackingGrid = roll

		local grid = pool[roll]
		for i, tbl in pairs(grid) do
			for j, val in pairs(tbl) do
				mod.HackingTiles[j][i]:SetFrame(val)

				if val == 1 then -- Player
					mod.HackingPlayer = Vector(j, i)
				end
			end
		end
	end
end

function mod.HackingTileActions(player, tileframe, forcefail)
	local data = player:GetData()

	if forcefail then
		mod.IsHacking = false
		data.blisscircuit = false
		player.ControlsEnabled = true

		player:AnimateSad()

		mod.PerformFailedHack(player)

		return
	end

	if tileframe == mod.HackingTileType.TILE_SPIKE then
		mod.IsHacking = false
		data.blisscircuit = false
		player.ControlsEnabled = true

		player:AnimateSad()

		if mod.HackIsFishing then
			mod.FishingRewards(player, mod.HackingTileType.TILE_SPIKE)
		else
			mod.PerformFailedHack(player)
		end
	elseif tileframe == mod.HackingTileType.TILE_DOOR then
		mod.GenerateHackingGrid(false, data.hackingcounter == 2)
		data.hackingcounter = data.hackingcounter + 1
	elseif tileframe == mod.HackingTileType.TILE_CROWN then
		mod.IsHacking = false
		data.blisscircuit = false
		player.ControlsEnabled = true

		player:AnimateHappy()

		mod.PerformSuccessfulHack(player)
	end

	for _, f in pairs(mod.HackingFish) do
		if type(f) == "table" then
			if mod.HackingPlayer.Y == f[1].Y and mod.HackingPlayer.X == f[1].X then
				mod.FishingRewards(player, f[4] and mod.HackingTileType.TILE_GOLDFISH or (f[3] and mod.HackingTileType.TILE_JELLY or mod.HackingTileType.TILE_FISH))
				break
			end
		end
	end
end

function mod.HackingResponse(player, dir)
	local move = Vector(0, 0)

	if mod.IsInMirror() then
		if dir == "left" then
			dir = "right"
		elseif dir == "right" then
			dir = "left"
		end
	end

	if dir == "left" then
		if mod.HackingPlayer.X <= 1 or mod.HackIsFishing then
			return
		end

		move = Vector(-1, 0)
	elseif dir == "right" then
		if mod.HackingPlayer.X >= 7 or mod.HackIsFishing then
			return
		end

		move = Vector(1, 0)
	elseif dir == "up" then
		if mod.HackingPlayer.Y <= 1 then
			return
		end

		move = Vector(0, -1)
	elseif dir == "down" then
		if mod.HackingPlayer.Y >= 7 then
			return
		end

		move = Vector(0, 1)
	end

	local pspr = mod.HackingTiles[mod.HackingPlayer.X][mod.HackingPlayer.Y]
	local tspr = mod.HackingTiles[mod.HackingPlayer.X + move.X][mod.HackingPlayer.Y + move.Y]

	local f = tspr:GetFrame()

	tspr:SetFrame(mod.HackIsFishing and mod.HackingTileType.TILE_HOOK or mod.HackingTileType.TILE_PLAYER)
	pspr:SetFrame(mod.HackingTileType.TILE_BLANK)
	mod.HackingPlayer = mod.HackingPlayer + move

	mod.HackingTileActions(player, f)
end

function mod.UpdateFish(player)
	local toremove = {}

	for i, fish in pairs(mod.HackingFish) do
		if fish[2] then
			fish[1] = Vector(fish[1].X - 1, fish[1].Y)
		else
			fish[1] = Vector(fish[1].X + 1, fish[1].Y)
		end

		if fish[1].X == mod.HackingPlayer.X then
			if fish[3] and fish[1].Y <= mod.HackingPlayer.Y then
				mod.FishingRewards(player, mod.HackingTileType.TILE_JELLY)
			elseif fish[1].Y == mod.HackingPlayer.Y then
				mod.FishingRewards(player, fish[4] and mod.HackingTileType.TILE_GOLDFISH or (fish[3] and mod.HackingTileType.TILE_JELLY or mod.HackingTileType.TILE_FISH))
			end
		end

		if (flip and fish[1].X == -1) or (fish[1].X == 9 and not flip) then
			toremove[#toremove + 1] = i
		end
	end

	for i = 1, #toremove do
		if mod.HackingFish[toremove[i] - i] then
			table.remove(mod.HackingFish, toremove[i] - i)
		else
			break
		end
	end

	if #mod.HackingFish == 0 then
		mod.CreateFish(nil, player)
	elseif #mod.HackingFish < 5 then
		mod.CreateFish(3, player)
	end
end

function mod.CreateFish(chance, player)
	local rng = Isaac.GetPlayer():GetCollectibleRNG(mod.COLLECTIBLES.BLISS_CIRCUIT)
	if not chance or (rng:RandomInt(chance) == rng:RandomInt(chance)) then
		local flip = rng:RandomInt(2) == rng:RandomInt(2)
		local roll = rng:RandomInt(10)
		
		local commongold = roll == 0
		local raregold = commongold and rng:RandomInt(10) < 5
		local dorare = not player:GetData().HackingTarget:ToPit()
		local cangold = not dorare or not mod.FishedWitnessKnife

		local gold = cangold and ((dorare and raregold) or (not dorare and commongold))


		local evil = not gold and roll <= 3

		local height = rng:RandomInt(evil and 4 or 5) + (evil and 3 or 2)

		mod.HackingFish[#mod.HackingFish + 1] = {Vector(flip and 7 or 1, height), flip, evil, gold}
	end
end

function mod.HasFish(player)
	local id = player:GetCard(0)
	if id then
		if mod.FishSprites[id] then
			return id
		else
			id = player:GetCard(1)
			if id and mod.FishSprites[id] then
				return id
			end
		end
	end

	return nil
end

function mod.GetClosestDoorSlot(player)
	local room = game:GetRoom()
	local closest
	local distance = 9999999

	for i = 0, 7 do
		if room:GetDoorSlotPosition(i):Distance(player.Position) < distance then
			closest = i
			distance = room:GetDoorSlotPosition(i):Distance(player.Position)
		end
	end

	return closest
end

function mod.IsDoorSlotAvailable(slot)
	local room = game:GetRoom()
	return room:IsDoorSlotAllowed(slot) and not room:GetDoor(slot)
end

mod.RoomIdShiftByDoorSlot = {
	[DoorSlot.LEFT0] = -1,
	[DoorSlot.LEFT1] = -1,
	[DoorSlot.RIGHT0] = 1,
	[DoorSlot.RIGHT1] = 1,
	[DoorSlot.UP0] = -13,
	[DoorSlot.UP1] = -13,
	[DoorSlot.DOWN0] = 13,
	[DoorSlot.DOWN1] = 13,

}

function mod.GetRoomIdShiftByDoorSlot(slot)
	return mod.RoomIdShiftByDoorSlot[slot]
end

function mod.GetDoorRoomIndex(slot, roomDesc)
	local safeIndex = roomDesc.SafeGridIndex
	local shape = roomDesc.Data.Shape

	if slot == DoorSlot.LEFT0 then
		return safeIndex
	elseif slot == DoorSlot.LEFT1 then
		if shape == RoomShape.ROOMSHAPE_LTL then
			return safeIndex + 12
		elseif shape == RoomShape.ROOMSHAPE_LBL then
			return safeIndex + 14
		else
			return safeIndex + 13
		end
	elseif slot == DoorSlot.RIGHT0 then
		if shape >= RoomShape.ROOMSHAPE_2x1 then
			if shape == RoomShape.ROOMSHAPE_LTR or shape == RoomShape.ROOMSHAPE_LTL then
				return safeIndex
			else
				return safeIndex + 1
			end
		else
			return safeIndex
		end
	elseif slot == DoorSlot.RIGHT1 then
		if shape >= RoomShape.ROOMSHAPE_2x1 then
			if shape == RoomShape.ROOMSHAPE_LTL or shape == RoomShape.ROOMSHAPE_LBR then
				return safeIndex + 13
			else
				return safeIndex + 14
			end
		else
			return safeIndex + 13
		end
	elseif slot == DoorSlot.UP0 then
		if shape == RoomShape.ROOMSHAPE_LTL then
			return safeIndex + 12
		else
			return safeIndex
		end
	elseif slot == DoorSlot.UP1 then
		if shape == RoomShape.ROOMSHAPE_LTL then
			return safeIndex
		elseif shape == RoomShape.ROOMSHAPE_LTR then
			return safeIndex + 14
		end
	elseif slot == DoorSlot.DOWN0 then
		if shape < RoomShape.ROOMSHAPE_2x2 then
			if shape == RoomShape.ROOMSHAPE_1x2 or shape == RoomShape.ROOMSHAPE_IIV then
				return safeIndex + 13
			else
				return safeIndex
			end
		elseif shape == RoomShape.ROOMSHAPE_LTL then
			return safeIndex + 12
		elseif shape == RoomShape.ROOMSHAPE_LBL then
			return safeIndex
		else
			return safeIndex + 13
		end
	elseif slot == DoorSlot.DOWN1 then
		if shape < RoomShape.ROOMSHAPE_2x2 or shape == RoomShape.ROOMSHAPE_LBR then
			return safeIndex + 1
		elseif shape == RoomShape.ROOMSHAPE_LTL then
			return safeIndex + 13
		else
			return safeIndex + 14
		end
	end
end

function mod.RoomExists(roomDesc)
	return roomDesc.GridIndex >= 0 or roomDesc.Data
end

function mod.GetTestDoorSlotLeadingToRoom(testRoomDesc, targetRoomIndex)
	local safeGridIndex = testRoomDesc.SafeGridIndex

	if safeGridIndex == targetRoomIndex + 13 then 		-- Above TL
		return 1 << DoorSlot.UP0
	elseif safeGridIndex == targetRoomIndex + 12 then 	-- Above TR
		return 1 << DoorSlot.UP1
	elseif safeGridIndex == targetRoomIndex + 1 then 	-- Left of TL
		return 1 << DoorSlot.LEFT0
	elseif safeGridIndex == targetRoomIndex - 12 then 	-- Left of BL
		return 1 << DoorSlot.LEFT1
	elseif safeGridIndex == targetRoomIndex - 1 then 	-- Right of TR
		return 1 << DoorSlot.RIGHT0
	elseif safeGridIndex == targetRoomIndex - 2 then 	-- Right of big TR
		return 1 << DoorSlot.RIGHT0
	elseif safeGridIndex == targetRoomIndex - 14 then 	-- Diagonal down-right of SafeIndex
		if testRoomDesc.Shape == RoomShape.ROOMSHAPE_LBR or testRoomDesc.Shape == RoomShape.ROOMSHAPE_1x2 then
			return 1 << DoorSlot.RIGHT1
		elseif testRoomDesc.Shape == RoomShape.ROOMSHAPE_2x1 then
			return 1 << DoorSlot.DOWN1
		end
	elseif safeGridIndex == targetRoomIndex - 15 then 	-- Right of big BR
		return 1 << DoorSlot.RIGHT1
	elseif safeGridIndex == targetRoomIndex - 13 then 	-- Below BL
		return 1 << DoorSlot.DOWN0
	elseif safeGridIndex == targetRoomIndex - 26 then 	-- Below big BL
		if testRoomDesc.Shape == RoomShape.ROOMSHAPE_LTL then
			return 1 << DoorSlot.DOWN1
		else
			return 1 << DoorSlot.DOWN0
		end
	elseif safeGridIndex == targetRoomIndex - 27 then 	-- Below big BR
		return 1 << DoorSlot.DOWN1
	end
end

function mod.GetRedRoomEntryDoorFromSlot(slot)
	return (slot + 2) % 4
end

function mod.IsDoorSlotRedRoomValid(slot)
	local slotValid = mod.IsDoorSlotAvailable(slot)

	if slotValid then
		local level = game:GetLevel()
		local currentRoomDesc = level:GetCurrentRoomDesc()
		local iteratorId = mod.GetDoorRoomIndex(slot, currentRoomDesc) + mod.GetRoomIdShiftByDoorSlot(slot)

		for i = 1, 7, 2 do
			local checkId = iteratorId + mod.GetRoomIdShiftByDoorSlot(i)
			local checkDesc = level:GetRoomByIdx(checkId)

			if mod.RoomExists(checkDesc) then
				print(slot, checkId, iteratorId)

				local expectedSlots = mod.GetTestDoorSlotLeadingToRoom(checkDesc, iteratorId)
				if checkDesc.Data.Doors & expectedSlots == 0 then
					return false
				end
			end
		end

		return true
	else
		return false
	end
end

function mod.MakeNewSpecialRoom(player, command, doorSkin, variableName)
	local slot = mod.GetClosestDoorSlot(player)
	local valid = mod.IsDoorSlotRedRoomValid(slot)

	if valid then
		local room = game:GetRoom()
		local level = game:GetLevel()

		level:MakeRedRoomDoor(level:GetCurrentRoomIndex(), slot)
		local door = room:GetDoor(slot)
		local redRoomIdx = door.TargetRoomIndex

		local sprite = door:GetSprite()
		local anim = sprite:GetAnimation()
		sprite:Load(doorSkin, true)
		sprite:Play(anim)

		sfx:Stop(SoundEffect.SOUND_UNLOCK00)

		mod[variableName] = {redRoomIdx, mod.GetRedRoomEntryDoorFromSlot(slot), player}
		Isaac.ExecuteCommand(command)

		return true
	else
		sfx:Play(SoundEffect.SOUND_BOSS2INTRO_ERRORBUZZ)
		return false
	end
end

function mod.MakeAngelFishRoom(player)
	local success = mod.MakeNewSpecialRoom(player, "goto s.angel", "gfx/grid/door_07_holyroomdoor.anm2", "TrackAngelFishRoom")
	if success then
		sfx:Play(SoundEffect.SOUND_CHOIR_UNLOCK)
	end

	return success
end

function mod.MakeDevilFishRoom(player)
	local success = mod.MakeNewSpecialRoom(player, "goto s.devil", "gfx/grid/door_07_devilroomdoor.anm2", "TrackDevilFishRoom")
	if success then
		sfx:Play(SoundEffect.SOUND_SATAN_ROOM_APPEAR)
	end

	return success
end

function mod.MakeStarfishRoom(player)
	return mod.MakeNewSpecialRoom(player, "goto s.planetarium", "gfx/grid/door_00x_planetariumdoor.anm2", "TrackStarfishRoom")
end

function mod.DoPostNewRoomMakeNewSpecialRoom(variableName)
	local level = game:GetLevel()
	local redRoomIdx = mod[variableName][1]
	local redRoomDesc = level:GetRoomByIdx(redRoomIdx)
	local currentRoomDesc = level:GetCurrentRoomDesc()
	local entryDoor = mod[variableName][2]

	redRoomDesc.Data = currentRoomDesc.Data
	redRoomDesc.Flags = currentRoomDesc.Flags

	mod[variableName] = nil

	game:ChangeRoom(redRoomIdx)

	local room = game:GetRoom()
	for _, player in pairs(Isaac.FindByType(1)) do
		local doorPos = room:GetDoorSlotPosition(entryDoor) 
		player.Position = doorPos + (room:GetCenterPos() - doorPos):Resized(30) 
	end
end

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	if mod.TrackAngelFishRoom then
		local player = mod.TrackAngelFishRoom[3]
		mod.DoPostNewRoomMakeNewSpecialRoom("TrackAngelFishRoom")
		player:AnimateCard(mod.CARDS.ANGELFISH)

		local room = game:GetRoom()
		Isaac.Spawn(38, 1, 0, room:GetGridPosition(64), Vector.Zero, nil)
		Isaac.Spawn(38, 1, 0, room:GetGridPosition(70), Vector.Zero, nil)

		room:SetClear(false)
		for i = DoorSlot.LEFT0, DoorSlot.DOWN0 do
			local door = room:GetDoor(i)
			if door then door:Close() end
		end
	elseif mod.TrackDevilFishRoom then
		local player = mod.TrackDevilFishRoom[3]
		mod.DoPostNewRoomMakeNewSpecialRoom("TrackDevilFishRoom")
		player:AnimateCard(mod.CARDS.DEVIL_RAY)

		local room = game:GetRoom()
		Isaac.Spawn(259, 0, 0, room:GetGridPosition(64), Vector.Zero, nil)
		Isaac.Spawn(259, 0, 0, room:GetGridPosition(70), Vector.Zero, nil)

		room:SetClear(false)
		for i = DoorSlot.LEFT0, DoorSlot.DOWN0 do
			local door = room:GetDoor(i)
			if door then door:Close() end
		end
	elseif mod.TrackStarfishRoom then
		local player = mod.TrackStarfishRoom[3]
		mod.DoPostNewRoomMakeNewSpecialRoom("TrackStarfishRoom")
		player:AnimateCard(mod.CARDS.STARFISH)
	end
end)

mod.HackingLayouts = {}
mod.HackingLayouts.Default, mod.HackingLayouts.Treasure = include("lua/meta/hackinglayouts")

mod.HackingTargets = {}
mod.HackingPickups = {}

mod.HackingPlayer = Vector.Zero
mod.HackingFish = {}
mod.LastHackingGrid = 0

mod.HackingBoard = Sprite()
mod.HackingFrame = Sprite()
mod.HackingOverlay = Sprite()
mod.HackingLine = Sprite()
mod.HackingFishSpr = Sprite()
--mod.HackingFishSpr2 = Sprite()
mod.HackingFishSpr3 = Sprite()
mod.HackingFishSpr4 = Sprite()

mod.HackingBoard:Load("gfx/ui/gui_hacking.anm2", true)
mod.HackingBoard:SetFrame("Board", 0)

mod.HackingFrame:Load("gfx/ui/gui_hacking.anm2", true)
mod.HackingFrame:SetFrame("Indicator", 0)
mod.HackingFrame:PlayOverlay("Border")

mod.HackingOverlay:Load("gfx/ui/gui_hacking.anm2", true)
mod.HackingOverlay:Play("Overlay")

mod.HackingLine:Load("gfx/ui/gui_hacking.anm2", true)
mod.HackingLine:Play("Line")

mod.HackingFishSpr:Load("gfx/ui/gui_hacking.anm2", true)
-- mod.HackingFishSpr2:Load("gfx/ui/gui_hacking.anm2", true)      A relic
mod.HackingFishSpr3:Load("gfx/ui/gui_hacking.anm2", true)
mod.HackingFishSpr4:Load("gfx/ui/gui_hacking.anm2", true)
mod.HackingFishSpr:SetFrame("Tile", mod.HackingTileType.TILE_FISH)
--mod.HackingFishSpr2:SetFrame("Tile", mod.HackingTileType.TILE_TAIL)
mod.HackingFishSpr3:SetFrame("Tile", mod.HackingTileType.TILE_JELLY)
mod.HackingFishSpr4:SetFrame("Tile", mod.HackingTileType.TILE_GOLDFISH)

mod.HackingTiles = {}
for i = 1, 7 do
	mod.HackingTiles[i] = {}

	for j = 1, 7 do
		mod.HackingTiles[i][j] = Sprite()

		mod.HackingTiles[i][j]:Load("gfx/ui/gui_hacking.anm2", true)
		mod.HackingTiles[i][j]:SetFrame("Tile", 0)
	end
end

mod:AddCallback(ModCallbacks.MC_POST_PLAYER_RENDER, function(_, p)
	if (mod.IsHacking and p:GetData().blisscircuit) or mod.HackingAppear then
		local position = Isaac.WorldToScreen(p.Position) - Vector(0, 60)
		position = Vector(position.X, math.max(50, position.Y))

		if p.Position.X < game:GetRoom():GetCenterPos().X then
			position = position + Vector(60, 0)
		else
			position = position - Vector(60, 0)
		end

		local offset = Vector(-32, -32)
		local unflip = mod.IsInMirror()

		--print(mod.IsMirrorStage())
		--print(mod.IsInDimension(1))

		--print(unflip)

		mod.HackingBoard.FlipX = unflip
		mod.HackingBoard:Render(position, Vector.Zero, Vector.Zero)

		if not mod.HackingAppear or mod.HackingBoard:GetFrame() == 13 then
			for i, tbl in pairs(mod.HackingTiles) do
				for j, spr in pairs(tbl) do
					if spr:GetFrame() ~= 0 then
						spr.FlipX = unflip
						spr:Render(position + offset + Vector(8 * i, 8 * j), Vector.Zero, Vector.Zero)
					end
				end
			end

			if mod.HackIsFishing then
				for i = 2, mod.HackingPlayer.Y - 1 do
					mod.HackingLine.FlipX = unflip
					mod.HackingLine:Render(position + offset + Vector(4 * 8, 8 * i), Vector.Zero, Vector.Zero)
				end

				for _, fish in pairs (mod.HackingFish) do
					if fish[1].X >= 1 and fish[1].X <= 7 then
						if fish[3] then
							mod.HackingFishSpr3.FlipX = unflip
							mod.HackingFishSpr3:Render(position + offset + fish[1] * 8, Vector.Zero, Vector.Zero)
						elseif fish[4] then
							mod.HackingFishSpr4.FlipX = fish[2]
							if not unflip then mod.HackingFishSpr4.FlipX = not mod.HackingFishSpr4.FlipX end
							mod.HackingFishSpr4:Render(position + offset + fish[1] * 8, Vector.Zero, Vector.Zero)
						else
							mod.HackingFishSpr.FlipX = fish[2]
							if not unflip then mod.HackingFishSpr.FlipX = not mod.HackingFishSpr.FlipX end
							mod.HackingFishSpr:Render(position + offset + fish[1] * 8, Vector.Zero, Vector.Zero)
						end
					end
				end
			end
		end

		if not mod.HackingAppear or mod.HackingBoard:GetFrame() >= 13 then
			mod.HackingOverlay.FlipX = unflip
			mod.HackingOverlay:Render(position, Vector.Zero, Vector.Zero)

			mod.HackingFrame.FlipX = unflip
			mod.HackingFrame:Render(position, Vector.Zero, Vector.Zero)
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, _, _, player)
	local data = player:GetData()

	if data.HackingReticle and data.HackingReticle.Visible then
		mod.IsHacking = true
		mod.HackIsFishing = not data.HackingTarget.Friction and (data.HackingTarget:ToPit() or data.HackingTarget:GetType() == GridEntityType.GRID_TRAPDOOR)
		mod.HackingFish = {}
		mod.GenerateHackingGrid(mod.HackIsFishing)

		--data.blisscircuit = true
		--data.hackingstartframe = player.FrameCount
		data.hackingcounter = 0
		data.inputracker = {}

		mod.HackingFrame:PlayOverlay("Border", true)

		player.ControlsEnabled = false
		player:AnimateCollectible(mod.COLLECTIBLES.BLISS_CIRCUIT, "LiftItem")

		mod.HackingAppear = player
		mod.HackingBoard:Play("Appear")
	else
		sfx:Play(SoundEffect.SOUND_BOSS2INTRO_ERRORBUZZ, 1, 0, false, 1)
		return {Discharge = false}
	end
end, mod.COLLECTIBLES.BLISS_CIRCUIT)

mod:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, player)
	local data = player:GetData()

	if data.blisscircuit then
		data.inputtracker = data.inputtracker or {}

		if not data.inputtracker.left and Input.IsActionPressed(ButtonAction.ACTION_LEFT, player.ControllerIndex) then
			mod.HackingResponse(player, "left")
		elseif not data.inputtracker.right and Input.IsActionPressed(ButtonAction.ACTION_RIGHT, player.ControllerIndex) then
			mod.HackingResponse(player, "right")
		elseif not data.inputtracker.up and Input.IsActionPressed(ButtonAction.ACTION_UP, player.ControllerIndex) then
			mod.HackingResponse(player, "up")
		elseif not data.inputtracker.down and Input.IsActionPressed(ButtonAction.ACTION_DOWN, player.ControllerIndex) then
			mod.HackingResponse(player, "down")
		end

		data.inputtracker.left = Input.IsActionPressed(ButtonAction.ACTION_LEFT, player.ControllerIndex)
		data.inputtracker.right = Input.IsActionPressed(ButtonAction.ACTION_RIGHT, player.ControllerIndex)
		data.inputtracker.up = Input.IsActionPressed(ButtonAction.ACTION_UP, player.ControllerIndex)
		data.inputtracker.down = Input.IsActionPressed(ButtonAction.ACTION_DOWN, player.ControllerIndex)

		if (data.hackingstartframe - player.FrameCount) % 20 == 19 then
			mod.HackingFrame:Update()
		end

		if mod.HackIsFishing and (data.hackingstartframe - player.FrameCount) % 7 == 0 then
			mod.UpdateFish(player)
		end

		if mod.HackingFrame:IsOverlayFinished("Border") then
			mod.IsHacking = false
			data.blisscircuit = false
			player.ControlsEnabled = true

			player:AnimateSad()

			if mod.HackIsFishing then
				mod.FishingRewards(player, mod.HackingTileType.TILE_SPIKE)
			else
				mod.PerformFailedHack(player)
			end
		end
	end

	if data.HackingReticle then
		data.HackingTarget = mod.GetClosestHackableTarget(player.Position)

		local slot = mod.PlayerHasActiveItem(player, mod.COLLECTIBLES.BLISS_CIRCUIT)
		local shouldshowtarget = mod.PlayerHasCard(player, mod.CARDS.TECHNO_FISH) or (slot and not player:NeedsCharge(slot))

		data.HackingReticle.Visible = data.HackingTarget and shouldshowtarget
		data.HackingReticle.RenderZOffset = -5000

		if data.HackingTarget then
			if not data.HackingTarget.Friction and data.HackingTarget:GetType() == GridEntityType.GRID_TRAPDOOR then
				data.HackingReticle:GetSprite():Play("Big")
			else
				data.HackingReticle:GetSprite():Play("Small")
			end

			data.HackingReticle.Position = data.HackingTarget.Position
		end
	end

	local hastechno = mod.PlayerHasCard(player, mod.CARDS.TECHNO_FISH)
	if not data.HadTechnoFish and hastechno then
		mod.CheckHackingTargets()
	end

	data.HadTechnoFish = hastechno
end)

mod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, player)
	if player:GetPlayerType() == mod.CHARACTERS.EIGHT and game.TimeCounter == 0 then
		if mod.BlissCircuitSlot == ActiveSlot.SLOT_POCKET then
			player:SetPocketActiveItem(mod.COLLECTIBLES.BLISS_CIRCUIT)
		else
			player:AddCollectible(mod.COLLECTIBLES.BLISS_CIRCUIT, 4, true, mod.BlissCircuitSlot)
		end
		player:AddNullCostume(mod.COSTUMES.EIGHT_HEAD)
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	if mod.IsHacking then
		mod.HackingOverlay:Update()
	end

	if mod.HackingAppear then
		mod.HackingBoard:Update()

		if mod.HackingBoard:IsFinished("Appear") then
			mod.HackingBoard:Play("Board")

			local data = mod.HackingAppear:GetData()
			data.blisscircuit = true
			data.hackingstartframe = mod.HackingAppear.FrameCount

			mod.HackingAppear = nil
		end
	end

	if mod.ShouldGatherHackingTargets then
		mod.ShouldGatherHackingTargets = false

		mod.DidGatherTargets = mod.AnyPlayerHas(mod.COLLECTIBLES.BLISS_CIRCUIT) or mod.AnyPlayerHasCard(mod.CARDS.TECHNO_FISH)
		mod.DidGatherTargets:GetData().HackingReticle = Isaac.Spawn(1000, mod.EFFECTS.HACKING_RETICLE, 0, Vector.Zero, Vector.Zero, nil)

		mod.GatherHackingTargets()
	end

	if mod.ReturnCharge then
		local player, item, slot, charge, blood, soul = table.unpack(mod.ReturnCharge)
		local maxcharge = Isaac.GetItemConfig():GetCollectible(item).MaxCharges

		player:SetActiveCharge(charge, slot)
		player:AddSoulCharge(maxcharge - charge)
		player:AddBloodCharge(maxcharge - charge)

		mod.ReturnCharge = nil
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	local room = game:GetRoom()

	local level = game:GetLevel()
	if room:IsFirstVisit() and level:GetCurrentRoomDesc().SafeGridIndex == level:GetRoomByIdx(level:GetStartingRoomIndex()).SafeGridIndex then
		-- New Level!! Some stuff needs to be done BEFORE new room

		mod.HackingLockedSecretDoors = {}
		mod.HackingOverrideCurse = nil
		mod.HackingOverrideCurseDouble = nil
		mod.CorpseHoleFishingCounter = 0
		mod.FishedWitnessKnife = false
	end

	if mod.HackingOverrideCurse or mod.HackingOverrideCurseDouble then
		local d
		for i = 0, 8 do
			if room:IsDoorSlotAllowed(i) then
				d = room:GetDoor(i)
				if d and d:IsRoomType(RoomType.ROOM_CURSE) then
					local sprite = d:GetSprite()

					if mod.HackingOverrideCurse and d.VarData == 0 then
						d.VarData = 1
						
						sprite:ReplaceSpritesheet(3, "gfx/grid/door_04_selfsacrificeroomdoor_nospikes.png")
						sprite:LoadGraphics()
					elseif mod.HackingOverrideCurseDouble then
						sprite:ReplaceSpritesheet(3, "gfx/grid/super_cursed_room.png")
						sprite:LoadGraphics()
					end
				end
			end
		end

		mod.HackingOverrideCurse = false
	end

	mod.HackingPickups = {}
	mod.HackingTargets = {}
	if mod.AnyPlayerHas(mod.COLLECTIBLES.BLISS_CIRCUIT) or mod.AnyPlayerHasCard(mod.CARDS.TECHNO_FISH) then
		if room:IsClear() then
			mod.ShouldGatherHackingTargets = true
		else
			if mod.DidGatherTargets then mod.DidGatherTargets:GetData().HackingReticle = nil end
			mod.DidGatherTargets = nil
		end
	end

	local roomidx = game:GetLevel():GetCurrentRoomDesc().SafeGridIndex

	if mod.HackingLockedSecretDoors[roomidx] then
		for _, slot in pairs(mod.HackingLockedSecretDoors[roomidx]) do
			local door = room:GetDoor(slot)
			local state = door:GetSaveState()
			state.Variant = 17

			local sprite = door:GetSprite()
			sprite:Load("gfx/grid/door_17_bardoor.anm2", true)
			sprite:Play("Idle")

			sprite.Offset = Vector(13, 0):Rotated(90 * door.Direction)

			door:Init(state.VariableSeed)
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_PRE_SPAWN_CLEAN_AWARD, function()
	mod.CheckHackingTargets()
end)

mod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, entity, amount, flags, source, cooldown)
	if not mod.DamageCancel then
		if mod.IsHacking then
			mod.IsHacking = false

			entity:GetData().blisscircuit = false
			entity:ToPlayer().ControlsEnabled = true
		end

		if mod.HackingOverrideCurseDouble and flags & DamageFlag.DAMAGE_CURSED_DOOR > 0 then
			mod.DamageCancel = true
			entity:TakeDamage(amount * 2, flags, source, cooldown)
			mod.DamageCancel = false

			return false
		end
	end
end, 1)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pickup)
	local data = pickup:GetData()
	if (mod.AnyPlayerHas(mod.COLLECTIBLES.BLISS_CIRCUIT) or mod.AnyPlayerHasCard(mod.CARDS.TECHNO_FISH)) and not data.DoneHackingCollect then 
		if (pickup.Variant == 100 and pickup.SubType ~= 0 and pickup.Price and (pickup.Price >= 0 or pickup.Price == -2)) or (pickup.Price and pickup.Price > 0) then
			data.DoneHackingCollect = true

			mod.HackingPickups[#mod.HackingPickups + 1] = pickup
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, function(_, pickup)
	if pickup:GetSprite():IsPlaying("Appear") and pickup.SpawnerType ~= 1 then

		if pickup.Variant == 30 and mod.AnyPlayerIs(mod.CHARACTERS.EIGHT) then
			local rng = Isaac.GetPlayer():GetCollectibleRNG(mod.COLLECTIBLES.BLISS_CIRCUIT)
			if rng:RandomInt(2) == rng:RandomInt(2) then
				pickup:Morph(5, 90, 2, true, true)
				return
			end
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, effect)
	local sprite = effect:GetSprite()
	local data = effect:GetData()

	if data.OffsetAccel then
		sprite.Offset = sprite.Offset + Vector(0, data.OffsetAccel)
		data.OffsetAccel = data.OffsetAccel + 1
	end

	if sprite.Offset.Y + data.OffsetAccel > 0 then
		if effect.SubType == 0 then -- Collectible
			local pickup = Isaac.Spawn(5, mod.PICKUPS.FLAT_COLLECTIBLE, data.item, effect.Position, effect.Velocity, nil)
			local ps = pickup:GetSprite()
			ps:SetFrame("Idle", sprite:GetFrame())
			ps:ReplaceSpritesheet(0, Isaac.GetItemConfig():GetCollectible(data.item).GfxFileName)
			ps:LoadGraphics()

			pickup.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
			pickup.SpriteRotation = sprite.Rotation
		elseif effect.SubType == 1 then -- Golden Chest
			local chest = Isaac.Spawn(5, 60, 0, effect.Position, Vector.Zero, nil)
			chest:Update()
			chest:Update()
		elseif effect.SubType == 2 then -- Card (Fish)
			local pickup = Isaac.Spawn(5, 300, data.fish, effect.Position, effect.Velocity, nil)
			pickup:GetSprite():SetFrame("Idle", sprite:GetFrame())

			sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS)

			pickup.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
			pickup.SpriteRotation = sprite.Rotation + 90
		end

		effect:Remove()
	end
end, mod.EFFECTS.THROWN_SPRITE)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pickup)
	pickup.Velocity = pickup.Velocity * 0.8
end, mod.PICKUPS.FLAT_COLLECTIBLE)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, function(_, pickup)
	local sprite = pickup:GetSprite()

	sprite:SetFrame("Idle", math.random(8) - 1)
	sprite:ReplaceSpritesheet(0, Isaac.GetItemConfig():GetCollectible(pickup.SubType).GfxFileName)
	sprite:LoadGraphics()
end, mod.PICKUPS.FLAT_COLLECTIBLE)

mod:AddCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, function(_, pickup, collider)
	local query = collider:ToPlayer()
	if query and not query:IsHoldingItem() then
		local player = query
		local item = Isaac.GetItemConfig():GetCollectible(pickup.SubType)
		local hud = game:GetHUD()

		player:QueueItem(item)
		player:AnimateCollectible(pickup.SubType, "Pickup", "PlayerPickupSparkle")
		if mod.FishedWitnessKnife and pickup.SubType == CollectibleType.COLLECTIBLE_MOMS_KNIFE then
			hud:ShowItemText("Mother's Knife", "She WAS gonna use this...")
		else
			hud:ShowItemText(item.Name, item.Description)
		end

		sfx:Play(SoundEffect.SOUND_POWERUP1, 1, 0, false, 1)

		pickup:Remove()
	end
end, mod.PICKUPS.FLAT_COLLECTIBLE)

mod:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, npc)
	if npc.Variant == 0 and mod.FishedWitnessKnife then
		if npc.State == 8 then npc.State = 3 end
	end
end, 912)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	local health = player:GetHearts() + player:GetSoulHearts()
	if health <= 1 then
		player:AddHearts(24)
		Isaac.Spawn(1000, 49, 0, player.Position + Vector(0, 5), Vector.Zero, nil).SpriteOffset = Vector(0, -14)
	else
		player:TakeDamage(1, DamageFlag.DAMAGE_NO_PENALTIES, EntityRef(player), 45)
	end

	player:AddCollectible(Isaac.GetItemIdByName("Blood Jelly"))

	sfx:Play(SoundEffect.SOUND_VAMP_GULP)
	-- Jellies ain't got no bones
	--sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
end, mod.CARDS.BLOOD_JELLY)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	for i = 1, 15 do
		local s = Isaac.Spawn(5, 20, 1, player.Position, RandomVector():Resized(math.random(2, 5)), nil):GetSprite()
		for i = 1, math.random(0, 5) do
			s:Update()
		end
	end

	player:AddCollectible(Isaac.GetItemIdByName("Gold Fish"))

	sfx:Play(SoundEffect.SOUND_VAMP_GULP)
	sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
end, mod.CARDS.GOLD_FISH)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	player:AddHearts(24)
	Isaac.Spawn(1000, 49, 0, player.Position + Vector(0, 5), Vector.Zero, nil).SpriteOffset = Vector(0, -14)

	player:AddCollectible(Isaac.GetItemIdByName("Grilled Fish"))

	sfx:Play(SoundEffect.SOUND_VAMP_GULP)
	sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
end, mod.CARDS.GRILLED_FISH)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	player:AddMaxHearts(2)
	player:AddHearts(2)

	player:AddCollectible(Isaac.GetItemIdByName("Hearty Fish"))

	sfx:Play(SoundEffect.SOUND_VAMP_GULP)
	sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
end, mod.CARDS.HEARTY_FISH)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	if not mod.DoubleGulper then
		local trink1, trink2 = player:GetTrinket(0), player:GetTrinket(1)

		if trink1 > 0 then player:TryRemoveTrinket(trink1) end
		if trink2 > 0 then player:TryRemoveTrinket(trink2) end

		local id = game:GetItemPool():GetTrinket()
		player:AddTrinket(id)

		player:UsePill(PillEffect.PILLEFFECT_GULP, PillColor.PILL_NULL)

		mod.DoubleGulper = true
		player:UseCard(mod.CARDS.GULPER_EEL, 0)
		mod.DoubleGulper = false

		if trink1 > 0 then player:AddTrinket(trink1) end
		if trink2 > 0 then player:AddTrinket(trink2) end

		player:AddCollectible(Isaac.GetItemIdByName("Gulper Eel"))

		sfx:Play(SoundEffect.SOUND_VAMP_GULP)
		sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
	end
end, mod.CARDS.GULPER_EEL)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	player:AddBoneHearts(2)

	player:AddCollectible(Isaac.GetItemIdByName("Dead Fish"))

	sfx:Play(SoundEffect.SOUND_VAMP_GULP)
	sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
end, mod.CARDS.DEAD_FISH)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	player:AddBlueFlies(10, player.Position, player)

	player:AddCollectible(Isaac.GetItemIdByName("Catfish"))

	sfx:Play(SoundEffect.SOUND_VAMP_GULP)
	sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
end, mod.CARDS.CATFISH)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	local data = player:GetData()
	local fish = not data.HackingTarget.Friction and (data.HackingTarget:ToPit() or data.HackingTarget:GetType() == GridEntityType.GRID_TRAPDOOR)

	if fish then
		mod.FishingRewards(player, mod.HackingTileType.TILE_FISH)
	else
		mod.PerformSuccessfulHack(player)
	end

	player:AddCollectible(Isaac.GetItemIdByName("Techno Fish"))

	sfx:Play(SoundEffect.SOUND_VAMP_GULP)
	sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
end, mod.CARDS.TECHNO_FISH)


mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player, flags)
	if mod.MakeAngelFishRoom(player) then
		player:AddCollectible(Isaac.GetItemIdByName("Angelfish"))

		sfx:Play(SoundEffect.SOUND_VAMP_GULP)
		sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
	else
		if flags & UseFlag.USE_MIMIC == 0 then
			player:AddCard(card)
		end
	end
end, mod.CARDS.ANGELFISH)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player, flags)
	if mod.MakeDevilFishRoom(player) then
		player:AddCollectible(Isaac.GetItemIdByName("Devil Ray"))

		sfx:Play(SoundEffect.SOUND_VAMP_GULP)
		sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
	else
		if flags & UseFlag.USE_MIMIC == 0 then
			player:AddCard(card)
		end
	end
end, mod.CARDS.DEVIL_RAY)

mod:AddCallback(ModCallbacks.MC_USE_CARD, function(_, card, player)
	if mod.MakeStarfishRoom(player) then
		player:AddCollectible(Isaac.GetItemIdByName("Starfish"))

		sfx:Play(SoundEffect.SOUND_VAMP_GULP)
		sfx:Play(SoundEffect.SOUND_SMB_LARGE_CHEWS_4)
	else
		if flags & UseFlag.USE_MIMIC == 0 then
			player:AddCard(card)
		end
	end
end, mod.CARDS.STARFISH)

mod:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, item, rng, player)
	local fish = mod.HasFish(player)
	if fish then
		player:UseCard(fish, UseFlag.USE_MIMIC)
	end

	return true
end, mod.COLLECTIBLES.SHINY_LURE)

mod:AddCallback(ModCallbacks.MC_PRE_USE_ITEM, function(_, item, _, player, _, slot)
	if slot == ActiveSlot.SLOT_POCKET and mod.PocketBlocker then
		mod.ReturnCharge = {player, item, slot, player:GetActiveCharge(slot), player:GetBloodCharge(), player:GetSoulCharge()}

		return {Discharge = false}
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	if mod.TouchedFish then
		mod.TouchedFish = false
		if sfx:IsPlaying(SoundEffect.SOUND_BOOK_PAGE_TURN_12) then
			sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS)
			sfx:Stop(SoundEffect.SOUND_BOOK_PAGE_TURN_12)
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, function(_, pickup, collider)
	if collider.Type == 1 and pickup.SubType >= mod.CARDS.BLOOD_JELLY and pickup.SubType <= mod.HighestSpecialFish then
		if collider:ToPlayer():CanPickupItem() then
			mod.TouchedFish = true
		end
	end
end, 300)

mod:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pickup)
	if pickup.SubType >= mod.CARDS.BLOOD_JELLY and pickup.SubType <= mod.HighestSpecialFish then
		if pickup:GetSprite():IsEventTriggered("DropSound") then
			sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS)
		end
	end
end, 300)